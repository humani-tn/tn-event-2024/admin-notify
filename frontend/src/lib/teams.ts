type Team = {
    name: string;
    color: string;
}

export const teams: Team[] = [
    { name: "Bleu", color: "#5b99c7" },
    { name: "Rouge", color: "#ed465b" },
    { name: "Orange", color: "#fe8325" },
    { name: "Violet", color: "#5b4886" },
];