import { Configuration } from '$lib/api';
import {
	StreamersApiFactory,
} from '$lib/api';
import { api } from '$lib/config';

export const streamerApi = () => {
	return StreamersApiFactory(
		new Configuration({
			basePath: api(),
		})
	);
};