package web

import (
	"poc-helloasso/internal/config"
	"poc-helloasso/internal/rbmq"

	"github.com/gin-gonic/gin"
)

func OptionalString(s string) *string {
	if s == "" {
		return nil
	}
	return &s
}

func Setup(g *gin.Engine) {
	conf := config.GetConfig()

	g.Static("/", "./www")

	g.NoRoute(func(c *gin.Context) {
		// If on base URL, redirect to index.html
		if c.Request.URL.Path == "/" {
			c.File("./www/index.html")
			return
		} else {
			c.Redirect(302, conf.BaseURL)
			return
		}
	})

	type Action struct {
		Msg string `json:"msg"`
		Key string `json:"key"`
	}

	g.POST("/", func(c *gin.Context) {
		var action Action
		if err := c.ShouldBindJSON(&action); err != nil {
			c.JSON(400, gin.H{"error": err.Error()})
			return
		}

		if action.Msg == "" {
			c.JSON(400, gin.H{"error": "missing message"})
			return
		}

		rbmq.SendMessage(action.Msg, action.Key)

		c.JSON(200, gin.H{"message": "ok"})
	})
}
