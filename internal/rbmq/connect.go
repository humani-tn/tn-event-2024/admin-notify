// Package rbmq provides a connection to RabbitMQ
// It also provides a function to get the channel
// And renew the connection if it's lost
package rbmq

import (
	"log"
	"os"
	"poc-helloasso/internal/config"

	amqp "github.com/rabbitmq/amqp091-go"
)

// We need to keep the connection and channel in memory to be able to reconnect
var channel *amqp.Channel
var connection *amqp.Connection

func Startup() {
	// Connect to RabbitMQ for the first time
	conn, ch := connect()
	channel = ch
	connection = conn

	// Start the watchdog
	go connectionWatchdog()
}

func connect() (conn *amqp.Connection, ch *amqp.Channel) {
	// Get the connection string from the environment
	cfg := config.GetConfig()

	// Connects to RabbitMQ
	conn, err := amqp.Dial(cfg.Rbmq.ConnectionString)
	if err != nil {
		if os.Getenv("DEBUG") == "true" {
			log.Printf("Failed to connect to RabbitMQ: %s\n", err)
		}
		return
	}

	// Creates a channel to communicate with RabbitMQ
	ch, err = conn.Channel()
	if err != nil {
		if os.Getenv("DEBUG") == "true" {
			log.Printf("Failed to connect to RabbitMQ: %s\n", err)
		}
		return
	}

	if os.Getenv("DEBUG") == "true" {
		log.Println("Connected to RabbitMQ")
	}

	return
}
