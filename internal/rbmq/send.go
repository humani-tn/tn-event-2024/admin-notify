package rbmq

import (
	"context"
	"poc-helloasso/internal/config"

	amqp "github.com/rabbitmq/amqp091-go"
	"github.com/sirupsen/logrus"
)

// SendMessage sends a message to the exchange
func SendMessage(message string, key string) error {
	cfg := config.GetConfig()

	logrus.Debug("Sending message to queue: ", string(message))

	for channel == nil {
		// reconnect
		connection, channel = connect()
	}

	return channel.PublishWithContext(
		context.Background(),
		cfg.Rbmq.ExchangeName,
		key,
		false,
		false,
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        []byte(message),
		},
	)
}
