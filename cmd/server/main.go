package main

import (
	"poc-helloasso/internal/config"
	"poc-helloasso/web"

	"github.com/gin-gonic/gin"
)

func main() {
	e := gin.Default()
	c := config.GetConfig()

	// CORS
	e.Use(func(c *gin.Context) {
		c.Header("Access-Control-Allow-Origin", c.Request.Header.Get("Origin"))
		c.Header("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE,OPTIONS")
		c.Header("Access-Control-Allow-Headers", "*")
		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(200)
		}
		c.Next()
	})

	web.Setup(e)

	e.Run(":" + c.Port)
}
